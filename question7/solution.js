function sumOfAllDigitsIsOddOrsumOfAllDigitsIsEven(num) {
  if (isNaN(num) ){
    console.log('number is required')
    return;
  }

  const numberStrings = String(num).split('');
  let total = 0;

  for ( let numberString of numberStrings) {
    total += Number(numberString);
  }

  const isEven = total %2 === 0;

  if (isEven) {
    return 'sumOfAllDigitsIsEven'
  }

  return 'sumOfAllDigitsIsOdd'
}

module.exports = sumOfAllDigitsIsOddOrsumOfAllDigitsIsEven;