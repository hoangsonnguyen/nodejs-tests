function convertToArray(obj) {
  const result = [];
  for ( let key in obj) {
    const keyAndValue = [key, obj[key]];
    result.push(keyAndValue);
  }
  return result;
}

module.exports = convertToArray;