function insertArrayInTheMiddle(arr1, arr2) {
  const insertedArray = [...arr1, ...arr2];
  const sortedArray = insertedArray.sort();
  return sortedArray;
}

module.exports = insertArrayInTheMiddle;