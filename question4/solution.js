function findMinAndMax(array) {
  let min = array[0];
  let max = array[0];

  for (let i = 1; i < array.length; i++) {
    const currentValue = array[i];
    if (min > currentValue) {
      min = currentValue;
    }

    if (max < currentValue) {
      max = currentValue;
    }
  }

  return {
    min,
    max,
  };
}

function canGetNestedInside(arr1, arr2) {
  const {
    min: minArrayOne,
    max: maxArrayOne,
  } = findMinAndMax(arr1);

  const {
    min: minArrayTwo,
    max: maxArrayTwo,
  } = findMinAndMax(arr2);
  
  if (minArrayOne <= minArrayTwo ) {
    return false;
  }

  if (maxArrayOne >= maxArrayTwo ) {
    return false;
  }

  return true;
}



module.exports = canGetNestedInside;