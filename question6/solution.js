
function checkUpperCaseLetter (letter) {
  const isUpperLetter = letter.charCodeAt() >= 65 && letter.charCodeAt() <= 90;
  return isUpperLetter
}

function moveAllUppercaseLetetrsToFirst(s) {
  const letters = s.split('');
  const lowerCaseLetters = [];
  const upperCaseLetters = [];

  for ( let letter of letters) {
    const isUpperLetter = checkUpperCaseLetter(letter);

    if (isUpperLetter) {
      upperCaseLetters.push(letter);
      continue;
    }
    lowerCaseLetters.push(letter);
  }

  const allLetters = [...upperCaseLetters, ...lowerCaseLetters]
  return allLetters.join('');
}
module.exports = moveAllUppercaseLetetrsToFirst;