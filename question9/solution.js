function getNumberOfLayers(rug) {
  const centralRowIndex = Math.floor( rug.length / 2 );
  const centralRow = rug[centralRowIndex];
  const lettersInCentralRow = centralRow.split('');
  const centralColumnIndex = Math.floor( centralRow.length / 2 );
  
  let layerNumber = 0;
  let previousLetter = '';

  for ( let i = 0; i <= centralColumnIndex; i++) {

    const currentLetter = lettersInCentralRow[i];
    if (currentLetter !== previousLetter) {
      layerNumber++;
    }
    
    previousLetter = lettersInCentralRow[i];
  }

  return layerNumber;
}

module.exports = getNumberOfLayers;