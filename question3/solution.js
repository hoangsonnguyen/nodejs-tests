function checkUpperCaseLetter (letter) {
  const isUpperLetter = letter.charCodeAt() >= 65 && letter.charCodeAt() <= 90;
  return isUpperLetter
}

function findTheLowerCaseWOrd(str) {
  const letters = str.split('');
  const lowerCaseLetters = [];

  for ( let letter of letters) {
    const isUpperLetter = checkUpperCaseLetter(letter);

    if (!isUpperLetter) {
      lowerCaseLetters.push(letter);
    }
  }

  return lowerCaseLetters.join('');
}


module.exports = findTheLowerCaseWOrd;