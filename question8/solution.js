function howManyDaysBetweenTwoDates(date1, date2) {
  const dateOneInMillisecond = date1.getTime();
  const dateTwoInMillisecond = date2.getTime();
  const differenceInMillisecond =  dateTwoInMillisecond - dateOneInMillisecond;
  const differenceInDays = differenceInMillisecond / (1000 * 3600 * 24); 

  return differenceInDays;
}

module.exports = howManyDaysBetweenTwoDates;